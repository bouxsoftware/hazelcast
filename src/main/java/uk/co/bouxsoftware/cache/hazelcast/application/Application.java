package uk.co.bouxsoftware.cache.hazelcast.application;

import org.apache.commons.cli.*;
import uk.co.bouxsoftware.cache.hazelcast.node.HazelcastConfiguration;
import uk.co.bouxsoftware.cache.hazelcast.node.HazelcastNode;

import java.util.Scanner;


public class Application {

    static final String MODE_ARG = "m";
    static final String MULTICAST_ARG = "u";
    static final String MULTICAST_GROUP_IPS_ARG = "i";
    static final String EXIT = "exit";

    private final String[] args;
    private final Scanner scanner;

    private HazelcastNode hazelcastNode;

    public Application(String[] args, Scanner scanner) {
        this.args = args;
        this.scanner = scanner;
    }

    public void startApplication() throws ParseException {
        HazelcastConfiguration configuration = loadHazelcastConfiguration();
        if (this.hazelcastNode != null) {
            this.hazelcastNode.stopNode();
        }
        this.hazelcastNode = new HazelcastNode(configuration);
        this.hazelcastNode.startNode();

        if (configuration.getRunningMode().equals(HazelcastConfiguration.RUNNING_MODE_CLIENT)) {
            startClientMode();
        }
    }

    private void startClientMode() {
        boolean run = true;
        while (run) {

            String operation = askUser("Operation (update|remove|list|exit)?: ");

            if (operation.equals("list")) {
                printElements();
            } else if (operation.equals("remove")) {
                operationRemove();
                printElements();
            } else if (operation.equals("update")) {
                operationUpdate();
                printElements();
            } else if (operation.equals("exit")) {
                stopNode();
                run = false;
            }
        }
    }

    private void stopNode() {
        this.hazelcastNode.stopNode();
    }

    private HazelcastConfiguration loadHazelcastConfiguration() throws ParseException {
        CommandLine cmd = getCommandLineParsedArgs(args);
        HazelcastConfiguration conf = new HazelcastConfiguration();
        conf.setEnableMulticast(Boolean.parseBoolean(cmd.getOptionValue(MULTICAST_ARG)));
        conf.setExecutionMode(cmd.getOptionValue(MODE_ARG));
        conf.setClusterIpAddresses(cmd.getOptionValues(MULTICAST_GROUP_IPS_ARG));

        return conf;
    }

    private CommandLine getCommandLineParsedArgs(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        return parser.parse(getCommandLineParserOptions(), args);
    }

    private static Options getCommandLineParserOptions() {
        Options options = new Options();

        options.addOption(MODE_ARG, true, "node|client");
        options.addOption(MULTICAST_ARG, true, "true|false");
        options.addOption(MULTICAST_GROUP_IPS_ARG, true, "ip1, ip2, ..., ipn");

        return options;
    }

    private void operationUpdate() {
        String key;
        String value;
        key = askUser("Key?: ");
        value = askUser("Value?: ");
        this.hazelcastNode.cachePut(key, value);
    }

    private void operationRemove() {
        String key;
        key = askUser("Key?: ");
        this.hazelcastNode.cacheRemove(key);
    }

    private void printElements() {
        System.out.println("# Cache List");
        System.out.println(this.hazelcastNode.printCachedElements());
        System.out.println("");
    }

    private String askUser(String message) {
        System.out.print(message);
        String value = this.scanner.nextLine();
        return value;
    }

}
