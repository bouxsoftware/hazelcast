package uk.co.bouxsoftware.cache.hazelcast.node;


public class HazelcastConfiguration {

    public static final String RUNNING_MODE_NODE = "node";
    public static final String RUNNING_MODE_CLIENT = "client";

    private boolean enableMulticast;
    private String[] clusterIpAddresses;
    private String executionMode;

    public boolean isEnableMulticast() {
        return enableMulticast;
    }

    public void setEnableMulticast(boolean enableMulticast) {
        this.enableMulticast = enableMulticast;
    }

    public String[] getClusterIpAddresses() {
        return clusterIpAddresses;
    }

    public void setClusterIpAddresses(String[] clusterIpAddresses) {
        this.clusterIpAddresses = clusterIpAddresses;
    }

    public String getRunningMode() {
        return executionMode;
    }

    public void setExecutionMode(String executionMode) {
        this.executionMode = executionMode;
    }
}
