package uk.co.bouxsoftware.cache.hazelcast.node;

import java.util.HashMap;
import java.util.Map;

class Cache {
    private final Map<String, Object> cacheData;

    public Cache() {
        this.cacheData = new HashMap<String, Object>();
    }

    public Map<String, Object> getCacheData() {
        return cacheData;
    }
}
