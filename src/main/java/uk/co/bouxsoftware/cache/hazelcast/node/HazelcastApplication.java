package uk.co.bouxsoftware.cache.hazelcast.node;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.Map;

class HazelcastApplication {

    public static final String CACHE_MAP = "CACHE";
    private final HazelcastConfiguration configuration;
    private Map<String, Object> cache;
    private HazelcastInstance instance;
    private boolean instanceIsRunning;

    public HazelcastApplication(HazelcastConfiguration hazelcastConfigurationLocal) {
        this.configuration = hazelcastConfigurationLocal;
    }

    public void startNode() {
        // If multicast is enabled
        // run in normal mode, discover nodes
        instance = createConfiguredInstance(configuration);
        loadCachedMaps(instance);
        this.instanceIsRunning = true;
    }

    public boolean isRunning() {
        return this.instanceIsRunning;
    }

    public boolean isRunningOnNodeMode() {
        return this.configuration.getRunningMode().equals(HazelcastConfiguration.RUNNING_MODE_NODE);
    }

    public void stopNode() {
        instance.shutdown();
        this.instanceIsRunning = false;
    }

    private HazelcastInstance createConfiguredInstance(HazelcastConfiguration hazelcastConfigurationLocal) {
        final Config config = createConfig(hazelcastConfigurationLocal);
        return Hazelcast.newHazelcastInstance(config);
    }

    private Config createConfig(HazelcastConfiguration hazelcastConfigurationLocal) {
        Config config = new Config();
        NetworkConfig network = config.getNetworkConfig();
        JoinConfig join = network.getJoin();

        final boolean multicastEnabled = hazelcastConfigurationLocal.isEnableMulticast();
        join.getMulticastConfig().setEnabled(multicastEnabled);
        join.getTcpIpConfig().setEnabled(!multicastEnabled);
        configMemberIps(hazelcastConfigurationLocal, join);

        network.setJoin(join);
        config.setNetworkConfig(network);

        return config;
    }

    private void configMemberIps(HazelcastConfiguration hazelcastConfiguration, JoinConfig join) {
        if (hazelcastConfiguration.getClusterIpAddresses() != null) {
            for (String memberIp : hazelcastConfiguration.getClusterIpAddresses()) {
                if (hazelcastConfiguration.isEnableMulticast()) {
                    // ips for trusting on multicasting
                    join.getMulticastConfig().addTrustedInterface(memberIp);
                } else {
                    // ips for tcp discovering only
                    join.getTcpIpConfig().addMember(memberIp);
                }
            }
        }
    }


    private void loadCachedMaps(HazelcastInstance instance) {
        cache = instance.getMap(CACHE_MAP);
    }

    public Object cacheGet(String key) {
        return this.cache.get(key);
    }

    public Object cachePut(String key, Object value) {
        return this.cache.put(key, value);
    }

    public boolean cacheContainsKey(String key) {
        return this.cache.containsKey(key);
    }

    public Object cacheContainsValue(Object value) {
        return this.cache.containsValue(value);
    }

    public boolean cacheIsEmpty() {
        return this.cache.isEmpty();
    }

    public Object cacheRemove(String key) {
        return this.cache.remove(key);
    }

    public String printCachedElements() {
        StringBuilder sb = new StringBuilder();

        this.cache.forEach((k, v) -> {
            sb.append(String.format("key=%s value=%s%n", k, v.toString()));
        });

        return sb.toString();
    }


}
