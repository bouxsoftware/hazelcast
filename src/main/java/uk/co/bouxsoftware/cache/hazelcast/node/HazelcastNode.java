package uk.co.bouxsoftware.cache.hazelcast.node;

public class HazelcastNode {
    private final HazelcastApplication hazelcastApplication;

    public HazelcastNode(HazelcastConfiguration hazelcastConfiguration) {
        this.hazelcastApplication = new HazelcastApplication(hazelcastConfiguration);
    }

    public void startNode() {
        this.hazelcastApplication.startNode();
        if(this.hazelcastApplication.isRunningOnNodeMode()){
            startVerboseMode();
        }
    }

    private void startVerboseMode() {
        // Lambda Runnable
        Runnable task2 = () -> {
            while (hazelcastApplication.isRunning()) {
                System.out.println(printCachedElements());
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        };

        // start the thread
        new Thread(task2).start();
    }

    public void stopNode() {
        this.hazelcastApplication.stopNode();
    }

    public void cachePut(String key, String value) {
        this.hazelcastApplication.cachePut(key, value);
    }

    public void cacheRemove(String key) {
        this.hazelcastApplication.cacheRemove(key);
    }

    public String printCachedElements() {
        return this.hazelcastApplication.printCachedElements();
    }
}
